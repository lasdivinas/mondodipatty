/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mdp.model;

/**
 *
 * @author PC-03
 */
public class Product {

    private int id;
    private String name;
    private float price;
    private String description;
    private String country;
    private String linkImg;

    // COSTRUTTORI
    public Product() {
    }

    public Product(int id, String name, float price, String description, String country, String linkImg) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.country = country;
        this.linkImg = linkImg;
    }

    // GETTER e SETTER
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLinkImg() {
        return linkImg;
    }

    public void setLinkImg(String linkImg) {
        this.linkImg = linkImg;
    }

    //TO STRING
    @Override
    public String toString() {
        return String.format("id: %s,name: %s, price: %s, description: %s, country: %s, linkImg: %s", this.id, this.name, this.description, this.country, this.linkImg);
    }

}
