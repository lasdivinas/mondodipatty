/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mdp.model;

/**
 *
 * @author PC-03
 */
public class Item {
    private int id;
    private Product product;
    private int quantity;
    private float total;

    public Item(int id, Product product, int quantity, float total) {
        this.id = id;
        this.product = product;
        this.quantity = quantity;
        this.total = total;
    }

    public Item() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
    @Override
    public String toString() {
        return String.format("id: %s, product: %s, quantity: %s, total: %s", this.id, this.product, this.quantity, this.total);
    }
  
}
