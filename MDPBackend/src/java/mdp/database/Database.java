/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mdp.database;

import java.sql.DriverManager;
import com.mysql.cj.jdbc.Driver;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.Properties;

/**
 *
 * @author PC-03
 */
public class Database {
    private static Database instance;
    private Properties properties;
    private final String uri = "jdbc:mysql://172.232.207.103:3306";
    
    // Costruttore della classe Database
    //(privato perchè singleton)
    // tenta la connessione e assegna le proprietà di connessione
    private Database(){
        try {
            DriverManager.registerDriver(new Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        // Istanzia un nuovo oggetto properties
        this.properties = new Properties();
        // assegnamo a properties i valori che ci servono
        // ATTENZIONE: questa parte potrebbe dover essere aggiornata per ogni computer
        this.properties.put("useSSL", "false");
        this.properties.put("allowPublicKeyRetrieval", "true");
        // REMEMBER: aggiorna il nome utente e la password!
        this.properties.put("user", "sa");
        this.properties.put("password", "Password1234!");
        this.properties.put("loginTimeout","10");
    }
    
    //metodo publico per raggiungiere istanza database
    public static Database getinstance()
    {
       if (instance == null) {
           instance = new Database();
       }
       return instance;
    } 
    // Metodo per connetterci al database
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(this.uri, this.properties);
    }
    
    // MAIN di debugging
    public static void main(String[] args) {
        Database db = new Database();
        try {
            db.getConnection(); 
        } catch (Exception e) {
            e.printStackTrace();
        }
       
    }
    
}
