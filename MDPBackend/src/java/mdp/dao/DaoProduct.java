/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mdp.dao;

import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import mdp.database.Database;
import mdp.model.Product;

/**
 *
 * @author PC-03
 */
public class DaoProduct {
    private Database db = Database.getinstance();
    
    public List<Product> readProducts() {
        List<Product> retvalue = new ArrayList<>();
        Product p = null;
        // stringa con query sql da eseguire
        String query = "SELECT * FROM mondodipatty.products;";
        
        // try catch per eseguire la query
        try {
            PreparedStatement ps = db.getConnection().prepareStatement(query);
            // inseriamo risultato della query in un ResultSet
            ResultSet tabella = ps.executeQuery();
            // leggiamo risultati della query
            while(tabella.next()) {
                p = new Product(
                        tabella.getInt("id_product"),
                        tabella.getString("name"),
                        tabella.getFloat("price"),
                        tabella.getString("description"),
                        tabella.getString("country"),
                        tabella.getString("link_img")
                );
                if (p != null) {
                    retvalue.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return retvalue;
    }
    
    public static void main (String[] args) {
        DaoProduct dao = new DaoProduct();
        System.out.println(dao.readProducts());
    }
    public Product getProduct(String id_Product) {
       Product  retvalue = null;
        
        // stringa con query sql da eseguire
        String query = "SELECT * FROM mondodipatty.products WHERE id_product ="+ id_Product + ";";
        
        // try catch per eseguire la query
        try {
            PreparedStatement ps = db.getConnection().prepareStatement(query);
            // inseriamo risultato della query in un ResultSet
            ResultSet tabella = ps.executeQuery();
            // leggiamo risultati della query
            if (tabella.next()){
                retvalue= new Product(
                        tabella.getInt("id_product"),
                        tabella.getString("name"),
                        tabella.getFloat("price"),
                        tabella.getString("description"),
                        tabella.getString("country"),
                        tabella.getString("link_img")
                );
            }
                
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return retvalue;
    }
    
}
