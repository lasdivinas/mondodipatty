/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mdp.dao;

import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import mdp.database.Database;
import mdp.model.Product;
import mdp.model.Item;

/**
 *
 * @author PC-03
 */
public class DaoItem {
    private Database db = Database.getinstance();
     
    public List<Item> readItems() {
        List<Item> retvalue = new ArrayList<>();
  
        Item i = null;
        // stringa con query sql da eseguire
        String query = "SELECT id_cart, p.id_product, quantity, total, p.name, p.price, p.description, p.country, p.link_img "
                + "FROM mondodipatty.cart as c INNER JOIN mondodipatty.products as p ON c.id_product = p.id_product;";
        
        try {
            PreparedStatement ps = db.getConnection().prepareStatement(query);
            // inseriamo risultato della query in un ResultSet
            ResultSet tabella = ps.executeQuery();
            // leggiamo risultati della query
            while(tabella.next()) {
                i = new Item(
                        tabella.getInt("id_cart"),
                        new Product(
                                tabella.getInt("p.id_product"),
                                tabella.getString("p.name"),
                                tabella.getFloat("p.price"),
                                tabella.getString("p.description"),
                                tabella.getString("p.country"),
                                tabella.getString("p.link_img")
                            ),
                        tabella.getInt("quantity"),
                        tabella.getFloat("total")
                );
                if (i != null) {
                    retvalue.add(i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return retvalue;
    }
    
    public boolean addItem(Item item) {
        boolean retvalue = false;
        String query = "INSERT INTO mondodipatty.cart (id_product, quantity, total) VALUES (?, ?, ?);";
        
        // try catch per inserimento
        try {
            // create PreparedStatement that will execute the query
            PreparedStatement ps = db.getConnection().prepareStatement(query);
            
            // Insert data into query
            ps.setInt(1, item.getProduct().getId());
            ps.setInt(2, item.getQuantity());
            ps.setFloat(3, item.getTotal());
            
            // Execute query and plare result in ResultSet
            int i = ps.executeUpdate();
            // Check if insert happened
            if (i > 0) {
                retvalue = true;
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return retvalue;
    }
    
     public static void main (String[] args) {
        DaoItem dao = new DaoItem();
        System.out.println(dao.readItems());
    }
    public boolean deleteItem(Item item) {
        boolean retvalue = false; 
        String query = "delete from mondodipatty.cart where id_cart = ?;";
        try{
            PreparedStatement ps = db.getConnection().prepareStatement(query);
            ps.setInt(1, item.getId());
            int i = ps.executeUpdate();
            if(i > 0){
                retvalue = true;
            }
            
            
        }
        catch(Exception e){
           e.printStackTrace();
            
        }
        return retvalue;
    }
    public boolean updateItem(Item item){
        boolean retvalue = false;
        String query = "update mondodipatty.cart set quantity = ?, total =? where id_cart =? ;";
        try{
            PreparedStatement ps = db.getConnection().prepareStatement(query);
            ps.setInt(1, item.getQuantity());
            ps.setFloat(2, item.getTotal());
            ps.setInt(3, item.getId());
            int i = ps.executeUpdate();
            if(i >0){
                retvalue = true;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return retvalue;
    }
    
}
