/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mdp.servlet;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.util.List;
import mdp.dao.DaoItem;
import mdp.dao.DaoProduct;
import mdp.model.Item;
import mdp.model.Product;
import mdp.model.Response;

/**
 *
 * @author PC-03
 */
@WebServlet(name = "ServletItem", urlPatterns = {"/cart"})
public class ServletItem extends HttpServlet {

    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        response.setHeader("Access-Control-Allow-Origin", "*");
        
        DaoItem dao = new DaoItem();
        
        List<Item> cart = dao.readItems();
        
        Jsonb jb = JsonbBuilder.create();
        
        String json = jb.toJson(cart);
        
        out.println(json);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Create reader to read request body
        BufferedReader in = request.getReader();
        PrintWriter out = response.getWriter();
        response.setHeader("Access-Control-Allow-Origin", "*");
        // Read json from request
        String json = in.readLine();
        
        // Create Jsonb to transform json string into java Object
        Jsonb jb = JsonbBuilder.create();
        Item item = jb.fromJson(json, Item.class);
        // Create a new dao and invoke addItem method
        DaoItem dao = new DaoItem();
        if(request.getParameter("action").equals("DELETE")){
            System.out.println("siamo nel delete");
            if(dao.deleteItem(item)){
                
                System.out.println("eliminata con successo");
            }
            else{
                System.out.println("qualcosa è andato storto");
            }
        }
        else if(request.getParameter("action").equals("UPDATE")){
            if(dao.updateItem(item)){
                System.out.println("aggiornato con successo");
            }
            else{
                System.out.println("qualcosa è andato storto");
            }
        }
        else{
            boolean already = false;
            boolean res;
            List<Item> cart = dao.readItems();
            Item oldItem = null;
            for (Item i: cart) {
                if (i.getProduct().getId() == item.getProduct().getId()) {
                    System.out.println("esiste già");
                    already = true;
                    oldItem = i;
                    break;
                }
            }
            if (already) {
                System.out.println("already true");
                Item newItem = new Item(
                        oldItem.getId(),
                        oldItem.getProduct(),
                        oldItem.getQuantity() + item.getQuantity(),
                        oldItem.getTotal() + item.getTotal()
                );
                res = dao.updateItem(newItem);
            }
            else {
                System.out.println("already false");
                res = dao.addItem(item);
            }
            
            // Print the response if result of add method is ok
            if (res){
                Response resp = new Response("OK", "aggiunto con successo");
                String jsonResp = jb.toJson(resp);
                out.println(jsonResp);
            }
            
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
