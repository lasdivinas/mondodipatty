SELECT * FROM players;
SELECT MAX(id_player) FROM players;

SELECT id_team FROM teams WHERE name = 'test2';

INSERT INTO teams (id_team, name) VALUES (1, 'test1')

INSERT INTO team_players (id_team, id_player) VALUES (1, 241),(1,492),(1,483),(1,506),(1,318);

SELECT p.id_player , p.name ,p.team, p.pos , p.value , p.min_ex , p.max_ex  FROM team_players tp  INNER JOIN teams t ON tp.id_team = t.id_team INNER JOIN players p ON p.id_player = tp.id_player WHERE tp.id_team = 2 ORDER BY p.id_player ;

SELECT tp.id_team , tp.id_player FROM team_players tp  INNER JOIN teams t ON tp.id_team = t.id_team ;

SELECT name FROM teams ;

INSERT INTO leagues (id_league, name) VALUES (1, 'nba1');

INSERT INTO lineups (id_team, id_player, role) VALUES (3, 62, 'C');

DELETE FROM lineups WHERE id_team = 3 AND id_player = 132;

SELECT id_player FROM lineups WHERE id_team = 3 AND role = 'C';

DELETE FROM team_players WHERE id_team=3 AND id_player= 525;

SELECT * FROM players WHERE name = 'DE''ANTHONY MELTON';

UPDATE players SET espn_id = 4997537 WHERE name = 'LANDRY SHAMET';

INSERT INTO nba_teams (id, code, name) VALUES (1, 'ATL', 'Atlanta Hawks');