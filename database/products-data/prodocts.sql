/*
-- Query: SELECT * FROM mondodipatty.products
LIMIT 0, 1000

-- Date: 2024-04-03 10:07
*/
USE mondodipatty;
INSERT INTO `products` (`id_product`,`name`,`price`,`description`,`country`,`link_img`) VALUES (1,'Zampogna',150.00,'Costituita da 4 o 5 canne che vengono collegate a un otre, o sacco che il musicista riempie d’aria e che andando in risonanza con le ance presenti sulle canne produce il suono tipico di questo strumento.','Italia','https://molise.guideslow.it/wp-content/uploads/sites/12/2020/08/zampogna.jpg');
INSERT INTO `products` (`id_product`,`name`,`price`,`description`,`country`,`link_img`) VALUES (2,'Bouzouki',300.00,'E\' strumento a corde, più precisamente a 3 o 4 corde doppie senza tasti.','Grecia','https://upload.wikimedia.org/wikipedia/commons/4/4d/Bouzouki_tetrachordo.jpg');
INSERT INTO `products` (`id_product`,`name`,`price`,`description`,`country`,`link_img`) VALUES (3,'Banjo Tenore',380.00,'Caratterizza per la presenza di 4 corde, con la caratteristica cassa di risonanza dalla forma tonda e piatta, su cui si innesta il manico.','Africa','https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/BluegrassBanjo.jpg/121px-BluegrassBanjo.jpg');
INSERT INTO `products` (`id_product`,`name`,`price`,`description`,`country`,`link_img`) VALUES (4,'Duduk',270.00,'E\' uno strumento a fiato in legno con 8 fori nella parte superiore e uno nella parte inferiore.','Armenia','https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Duduk-view.jpg/640px-Duduk-view.jpg');
INSERT INTO `products` (`id_product`,`name`,`price`,`description`,`country`,`link_img`) VALUES (5,'Mandolino',450.00,'Composto di una cassa dalla forma quasi sempre arrotondata, con una serie di 4 corde doppie.','Italia','https://upload.wikimedia.org/wikipedia/commons/2/2b/Mandolin_MET_DP169023_%282%29.jpg');
INSERT INTO products (name, price, description, country, link_img) VALUES ('Shamisen', '200', 'Un liuto a tre corde con un suono inconfondibile, utilizzato nella musica tradizionale giapponese come il teatro Kabuki e Bunraky', 'Giappone', 'https://upload.wikimedia.org/wikipedia/commons/b/be/Shamisen_compare.JPG');