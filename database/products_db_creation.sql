create database mondodipatty;
use mondodipatty;
create table products (
	id_product INT PRIMARY KEY AUTO_INCREMENT,
    name NVARCHAR(100),
    price DECIMAL(5,2),
    description NVARCHAR(255),
    country NVARCHAR(255),
    link_img VARCHAR(255)
);
select * from products;
