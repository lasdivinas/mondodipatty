import { Component } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { AppModule } from './app/app.module';
import { HttpClient } from '@angular/common/http';
import { Item } from './model/item.model';
import { CartService } from './cart.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, AppModule, RouterModule],
  providers: [CartService],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'MDPFrontend';
  private uri = 'http://localhost:8080/mdpbackend/cart';

  protected totQty = 0;
  constructor(protected http: HttpClient, protected service: CartService) {
    this.service
      .getCart()
      .subscribe((res) =>
        res.forEach((item) => (this.service.totalItems += item.quantity))
      );
  }
}
