import { Component } from '@angular/core';
import { Item } from '../model/item.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',

  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css',
})
export class CartComponent {
  protected cart!: Item[];

  protected totQty = 0;
  protected totPrice = 0.0;
  protected updatingItem: Item | undefined;

  private uri = 'http://localhost:8080/mdpbackend/cart';

  private getMethod() {
    this.http.get<Item[]>(this.uri).subscribe((res) => {
      this.cart = res;
      this.cart.forEach((item) => console.log(item));
      this.cart.forEach((item) => {
        this.totQty += item.quantity;
        this.totPrice += item.total;
      });
      this.service.totalItems = this.totQty;
    });
  }

  private updateTotItems(item: Item) {
    this.service.totalItems += item.quantity;
  }

  constructor(protected http: HttpClient, protected service: CartService) {
    this.getMethod();
  }

  protected delItem(item: Item) {
    //const delParams = new HttpParams().set('action', 'DELETE');
    const json = JSON.stringify(item);

    this.http.post(this.uri + '?action=DELETE', json).subscribe(() => {
      this.totQty = 0;
      this.totPrice = 0.0;
      this.getMethod();
      this.updateTotItems(item);
    });
  }

  protected calcTotal(item: Item) {
    item.total = item.product.price * item.quantity;
  }

  protected updItem(item: Item) {
    if (!this.updatingItem) {
      this.updatingItem = item;
    } else {
      // reset the updating item to undefined
      this.updatingItem = undefined;
      const json = JSON.stringify(item);
      this.http.post(this.uri + '?action=UPDATE', json).subscribe(() => {
        this.totQty = 0;
        this.totPrice = 0.0;
        this.getMethod();
        this.updateTotItems(item);
      });
    }
  }
}
