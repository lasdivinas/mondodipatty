import { Injectable } from '@angular/core';
import { Item } from './model/item.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  public totalItems = 0;
  public cart: Item[] = [];
  protected uri = 'http://localhost:8080/mdpbackend/cart';

  public getCart(): Observable<Item[]> {
    return this.http.get<Item[]>(this.uri);
  }

  constructor(protected http: HttpClient) {}
}
