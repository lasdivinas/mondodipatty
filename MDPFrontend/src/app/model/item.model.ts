import { Product } from "./product.model";

export interface Item{
    id: number;
    product: Product;
    quantity: number;
    total: number;

}