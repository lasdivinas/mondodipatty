export interface Product {
    id: number;
    name: string;
    price: number;
    description: string;
    country: string;
    linkImg: string;
}