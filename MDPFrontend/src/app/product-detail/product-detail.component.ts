import { HttpClient, HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../model/product.model';
import { Item } from '../model/item.model';
import { MyResp } from '../model/myresp.model';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrl: './product-detail.component.css',
})
export class ProductDetailComponent {
  protected id_product!: number;

  protected product!: Product;

  protected quantity: number = 1;

  protected total!: number;

  protected added: boolean = false;

  private uri = 'http://localhost:8080/mdpbackend/detail';

  private cartUri = 'http://localhost:8080/mdpbackend/cart?action=ADD';

  constructor(
    protected http: HttpClient,
    protected route: ActivatedRoute,
    protected service: CartService
  ) {
    this.id_product = this.route.snapshot.queryParams['id_product'];
    this.http
      .get<Product>(this.uri + `?id_product=${this.id_product}`)
      .subscribe((res) => {
        // assegnamo al catalogo la risposta dalla servlet
        this.product = res;
        this.total = this.product.price;
        // test di lettura del catalogo, in console
        // console.log(this.product);
      });
  }

  protected calcTotal() {
    this.total = this.quantity * this.product.price;
  }

  protected addItem() {
    // Create Item to send to servlet
    const item: Item = {
      id: 0,
      product: this.product,
      quantity: this.quantity,
      total: this.total,
    };
    // Transform Item into json
    const json = JSON.stringify(item);
    // Send POST request to servlet
    this.http.post<MyResp>(this.cartUri, json).subscribe((res: MyResp) => {
      // Check if response is OK
      if (res.message == 'OK') {
        // Set the added property to true
        this.added = true;
      }

      // set a timeout to bring back added to false
      setTimeout(() => {
        this.added = false;
      }, 2200);

      this.service.totalItems += item.quantity;
    });
  }
}
