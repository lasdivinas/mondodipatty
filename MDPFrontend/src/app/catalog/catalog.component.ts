import { Component, EventEmitter, Output } from '@angular/core';
import { Product } from '../model/product.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { filter } from 'rxjs';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrl: './catalog.component.css',
})
export class CatalogComponent {
  // creiamo la lista che ospiterà i prodotti del catalogo
  protected catalog!: Product[];
  // backup of catalog for filtering
  protected bakCatalog!: Product[];

  // lista delle countries
  protected countryList: string[] = [];
  // selected country to filter by
  protected filter!: string;

  // creiamo la uri dove esponiamo la servlet
  private uri = 'http://localhost:8080/mdpbackend/products';

  // proprietà che indica il prodotto selezionato
  protected selected!: Product;

  // Costruttore
  // qui eseguiamo la GET request all'indirizzo della servlet
  constructor(protected http: HttpClient, protected router: Router) {
    this.http.get<Product[]>(this.uri).subscribe((res) => {
      // assegnamo al catalogo la risposta dalla servlet
      this.catalog = res;
      // set the backup catalog to original fetched catalog
      this.bakCatalog = res;
      // test di lettura del catalogo, in console
      this.catalog.forEach((product) => {
        if (!this.countryList.find((country) => country == product.country)) {
          this.countryList.push(product.country);
        }
      });
    });
  }
  //navigazione al dettaglio
  protected goToDetail(id_product: number) {
    this.router.navigate(['/detail'], {
      queryParams: { id_product: id_product },
    });
  }

  // metodo che filtra catalogo per country
  protected filterByCountry(filterCountry: string) {
    this.filter = filterCountry;

    // restore the entire catalog
    this.catalog = this.bakCatalog;
    // if filterCountry is not 'none', do the filtering
    if (!(filterCountry == 'none')) {
      this.catalog = this.catalog.filter(
        (product) => product.country == filterCountry
      );
    }
  }
}
