import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Item } from '../model/item.model';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrl: './purchase.component.css',
})
export class PurchaseComponent {
  protected cart: Item[] = [];
  protected totQty = 0;
  protected totPrice = 0.0;

  private uri = 'http://localhost:8080/mdpbackend/cart';

  private getMethod() {
    this.http.get<Item[]>(this.uri).subscribe((res) => {
      this.cart = res;
      this.cart.forEach((item) => console.log(item));
      this.cart.forEach((item) => {
        this.totQty += item.quantity;
        this.totPrice += item.total;
      });
    });
  }

  constructor(protected http: HttpClient) {
    this.getMethod();
  }
}
