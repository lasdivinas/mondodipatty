import { NgModule, importProvidersFrom } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogComponent } from '../catalog/catalog.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CartComponent } from '../cart/cart.component';
import { HomeComponent } from '../home/home.component';
import { CartService } from '../cart.service';
import { PurchaseComponent } from '../purchase/purchase.component';

@NgModule({
  declarations: [
    CatalogComponent,
    ProductDetailComponent,
    CartComponent,
    HomeComponent,
    PurchaseComponent,
  ],
  imports: [CommonModule, RouterModule, FormsModule],
  exports: [
    CatalogComponent,
    ProductDetailComponent,
    CartComponent,
    HomeComponent,
    PurchaseComponent,
  ],
  providers: [importProvidersFrom(HttpClientModule), CartService],
})
export class AppModule {}
