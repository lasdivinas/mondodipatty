import { Routes } from '@angular/router';

import { CatalogComponent } from './catalog/catalog.component';

import { ProductDetailComponent } from './product-detail/product-detail.component';
import { CartComponent } from './cart/cart.component';
import { HomeComponent } from './home/home.component';
import { ContactsComponent } from './contacts/contacts.component';
import { PurchaseComponent } from './purchase/purchase.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'catalog', component: CatalogComponent },
  { path: 'detail', component: ProductDetailComponent },
  { path: 'cart', component: CartComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'purchase', component: PurchaseComponent },
];
